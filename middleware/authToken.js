const jwt = require('jsonwebtoken');
const User = require('../models/User.model');

exports.verifyUser = async (req, res, next) => {
    const token =req.body.token || req.query.token || req.headers["x-access-token"];
    if (!token) {
      return res.status(403).send("Unauthorized");
    }
    try {
      const decoded = jwt.verify(token, 'secretKey');
      const user = await User.findById(decoded.id, { password: 0 })
      if (!user) return res.status(404).json({
            message: "Unauthorized"
      })
    } catch (err) {
      return res.status(401).send("Unauthorized");
    }
    return next();
};

exports.verifyAdmin = async (req, res, next) => {
    const token =req.body.token || req.query.token || req.headers["x-access-token"];
    if (!token) {
      return res.status(403).send("Unauthorized");
    }
    try {
      const decoded = jwt.verify(token, 'secretKey');
      const user = await User.findById(decoded.id, { password: 0 })
      if (user.role != "admin") return res.status(404).json({
            message: "Not unauthorized, please contact your support"
      })
    } catch (err) {
      return res.status(401).send("Unauthorized");
    }
    return next();
};

