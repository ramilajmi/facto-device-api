const Device = require('../models/Device.model');

exports.findAllDevices = async (req, res) => {
    console.log("tooooo");
    try {
        const devices = await Device.find();
        res.json(devices)
    } catch (error) {
        res.status(500).json({
            message: error.message || "Something goes wrong retieving the tasks"
        })
    }
};

exports.createDevice = async (req, res) => {
    try {
        const newDevice = new Device({
            name: req.body.name,
            brand: req.body.brand,
            os: req.body.os,
            registered_at: req.body.registered_at,
            serial: req.body.serial,  
            office: req.body.office,
            comments: req.body.comments
        });
        const deviceSaved = await newDevice.save();
        res.status(200).json({
            code: 200,
            message: "Created"
        })
    } catch (error) {
        res.status(500).json({
            message: error.message || "Something goes wrong creating a device"
        })
    }
};

exports.findOneDevice = async (req, res) => {
    console.log("test 1 "+req.params.id);
    try {
        console.log(req.params.id);
        const device = await Device.findOne({ serial: req.params.id })
        if(!device) return res.status(404).json({
            code: 400,
            message: `Device with id ${req.params.id} does not exists!`,
            device: null
        });
        res.status(200).json({
            code:200,
            message: "Yes",
            device: device
        })
    } catch (error) {
        res.status(500).json({
            code:500,
            message: error.message || `Error retrieving device with id: ${req.params.id}`,
            device: null
        })
    }
};

exports.deleteDevice = async (req, res) => {
    const { id } = req.params;
    try {
        const data = await Device.findByIdAndDelete(id)
        res.json({
            message: `${data.name} - Device were deleted successfully!`
        })
    } catch (error) {
        res.status(500).json({
            message: `Cannot delete device with id ${id}`
        })
    }
}

exports.updateDevice = async (req, res) => {
    const { serial } = req.params;
    try {
        await Device.updateOne({ serial: serial }, req.body)
    res.json({
        message: "Device was updated successfully"
    })
    } catch (error) {
        res.status(500).json({
            message: `Cannot update device with id: ${serial}`
        })
    }
}