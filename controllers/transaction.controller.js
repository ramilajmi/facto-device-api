const Transaction = require('../models/Transaction.model');

exports.findAllTransactions = async (req, res) => {
    try {
        const transaction = await Transaction.find();
        res.json(transaction)
    } catch (error) {
        res.status(500).json({
            message: error.message || "Something goes wrong retieving the tasks"
        })
    }
};

exports.findSpecificTransaction = async (req, res) => {
    try {
        const transaction = await Transaction.findOne({ serial: req.body.serial, status: req.body.status })
        if(!transaction) return res.status(404).json({
            code: 404,
            message: `Transaction with id ${req.params.id} does not exists!`,
            transaction: null
        });
        res.status(200).json({
            code:200,
            message: "Yes",
            transaction: transaction
        })
    } catch (error) {
        res.status(500).json({
            code: 500,
            message: error.message || `Error retrieving transaction with id: ${req.params.id}`,
            transaction: null
        })
    }
};

exports.createTransaction = async (req, res) => {
    try {
        const newTransaction = new Transaction({
            user_id: req.body.user_id,
            device_id: req.body.device_id,
            started_at: req.body.started_at,
            ended_at: req.body.ended_at,
            office: req.body.office,
            usage_place: req.body.usage_place,
            usage_subject: req.body.usage_subject,
            status: req.body.status,
            comments: req.body.comments
        });
        const transactionSaved = await newTransaction.save();
        res.json(transactionSaved)
    } catch (error) {
        res.status(500).json({
            message: error.message || "Something goes wrong creating a transaction"
        })
    }
};

exports.findOneTransaction = async (req, res) => {
    try {
        console.log(req.params.id);
        const transaction = await Transaction.findOne({ serial: req.params.id })
        if(!transaction) return res.status(404).json({
            code: 404,
            message: `Transaction with id ${req.params.id} does not exists!`,
            transaction: null
        });
        res.status(200).json({
            code:200,
            message: "Yes",
            transaction: transaction
        })
    } catch (error) {
        res.status(500).json({
            code: 500,
            message: error.message || `Error retrieving transaction with id: ${req.params.id}`,
            transaction: null
        })
    }
};

exports.deleteTransaction = async (req, res) => {
    const { id } = req.params;
    try {
        const data = await Transaction.findByIdAndDelete(id)
        res.json({
            message: `${data.name} - Transaction were deleted successfully!`
        })
    } catch (error) {
        res.status(500).json({
            message: `Cannot delete transaction with id ${id}`
        })
    }
}

exports.updateTransaction = async (req, res) => {
    const {id} = req.params;
    try {
        await Transaction.findByIdAndUpdate(id, req.body)
    res.json({
        message: "Transaction was updated successfully"
    })
    } catch (error) {
        res.status(500).json({
            message: `Cannot update transaction with id: ${id}`
        })
    }
}