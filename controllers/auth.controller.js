const User = require('../models/User.model');
const jwt = require('jsonwebtoken');

//Register User inside database 
exports.signUp = async (req, res) => {
    const { firstname, lastname, email, address, phone, position, office, role, password } = req.body;

    const newUser = new User({
        firstname, lastname, email, address, phone, position, office, role, password: await User.encryptPassword(password)
    })

    const savedUser = await newUser.save();
    console.log(savedUser);

    const newToken = jwt.sign({ id: savedUser._id }, 'secretKey', {
        expiresIn: 86400 // one day
    })

    res.status(200).json({ newToken })
}

//User login form database with credentials
exports.logIn = async (req, res) => {
    const userExist = await User.findOne({ email: req.body.email });

    if (!userExist) return res.status(400).json({
        message: 'Please use a valid credentials'
    })

    const matchPassword = await User.comparePassword(req.body.password, userExist.password)

    if (!matchPassword) return res.status(401).json({
        token: null,
        message: 'Please use a valid credentials'
    })

    const token = jwt.sign({ id: userExist._id }, 'secretKey', {
        expiresIn: 86400
    })

    return res.json({
        _id: userExist._id,
        firstname: userExist.firstname,
        lastname: userExist.lastname,
        email: userExist.email,
        message: 'Auth Succesful',
        token: token
    })
}
