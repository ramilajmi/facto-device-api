const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const pkg = require('./package.json');
const createError = require("http-errors");
const config = require('./config');
const env = process.env.NODE_ENV || 'development';

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(config.db[env], config.dbParams);

mongoose.connection.on("error", err => {
    console.log("err", err)
});
  
mongoose.connection.on("connected", () => {
  console.log("mongoose is connected...")
});

mongoose.connection.on("disconnected", () => {
  console.log("mongoose is disconnected...")
});

const authRoutes = require('./routes/auth.route');
const deviceRoutes = require('./routes/device.routes');
const transactionRoutes = require('./routes/transaction.routes');

// Routes
app.use('/api/auth', authRoutes);
app.use('/api/device', deviceRoutes);
app.use('/api/transaction', transactionRoutes);

// Welcome Route
app.get('/', (req, res) => {
    res.json({
        author: app.get('pkg').author,
        name: app.get('pkg').name,
        description: app.get('pkg').description,
        version:app.get('pkg').version
    })
})

app.use((req, res, next) => {
  next(createError(404));
});

app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.status(err.status || 500);
  res.send(err);
});

app.options('*', cors());

module.exports = app;