//Database configuration file
module.exports = {
    port: 4000,
    db: {
      production: "mongodb://user:devops@suisymps.com:1234/buterm",
      development: "mongodb://localhost:27017/buterm",
      test: "mongodb://localhost:27017/butermtest",
    },
    dbParams: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
};