const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//User Schema
const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true,
        trim: true
    },
    lastname: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    address: {
        type: String,
        required: true,
        trim: true
    },
    phone: {
        type: String,
        required: true,
        trim: true
    },
    position: {
        type: String,
        required: true,
        trim: true
    },
    office: {
        type: String,
        required: true,
        trim: true
    },
    role: {
        type: String, 
        enum: ['admin', 'user'], 
        default: 'user',
        trim: true
    },
    password: {
        type: String,
        required: true,
        trim: true
    }
});

// Hash password before save in DB
userSchema.statics.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10)
    return await bcrypt.hash(password, salt)
};

//Compare password 
userSchema.statics.comparePassword = async (password, receivedPassword) => {
    return await bcrypt.compare(password, receivedPassword)
};

module.exports = mongoose.model('User', userSchema);