const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//Terminal Schema
const transactionSchema = new mongoose.Schema({
    user_email: {
        type: String,
        required: true,
        trim: true
    },
    device_serial: {
        type: String,
        required: true,
        trim: true
    },
    started_at: {
        type: Date,
        default: new Date(),
        required: true,
        trim: true
    },
    ended_at: {
        type: Date,
        required: false,
        trim: true
    },
    office: {
        type: String,
        required: true,
        trim: true
    },
    usage_place: {
        type: String,
        required: true,
        trim: true
    },
    usage_subject: {
        type: String,
        required: true,
        trim: true
    },
    status: {
        type: String, 
        enum: ['available', 'not available'], 
        default: 'available',
        trim: true
    },
    comments: {
        type: String,
        required: false,
        trim: true
    }
});

module.exports = mongoose.model('Transaction', transactionSchema);