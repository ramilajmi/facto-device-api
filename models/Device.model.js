const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//Terminal Schema
const deviceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    brand: {
        type: String,
        required: true,
        trim: true
    },
    os: {
        type: String,
        required: true,
        trim: true
    },
    registered_at: {
        type: Date,
        default: new Date(),
        required: true,
        trim: true
    },
    serial: {
        type: String,
        required: true,
        trim: true
    },
    office: {
        type: String,
        required: true,
        trim: true
    },
    comments: {
        type: String,
        required: false,
        trim: true
    }
});

module.exports = mongoose.model('Device', deviceSchema);