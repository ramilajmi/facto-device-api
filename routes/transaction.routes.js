const express = require('express');

const router = express.Router();

const transactionController = require('../controllers/transaction.controller');
const authToken = require('../middleware/authToken');

router
  .route("/")
  .get([authToken.verifyUser], transactionController.findAllTransactions)
  .post([authToken.verifyUser], transactionController.createTransaction)
router
  .route("/:id")
  .get([authToken.verifyUser], transactionController.findOneTransaction)
  .put([authToken.verifyUser], transactionController.updateTransaction)
  .delete([authToken.verifyUser], transactionController.deleteTransaction);
router
  .route("/only")
  .post([authToken.verifyUser], transactionController.findSpecificTransaction)

module.exports = router;