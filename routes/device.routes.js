const express = require('express');

const router = express.Router();

const deviceController = require('../controllers/device.controller');
const authToken = require('../middleware/authToken');

router
  .route("/")
  .get([authToken.verifyUser], deviceController.findAllDevices)
  .post([authToken.verifyAdmin], deviceController.createDevice);
router
  .route("/:id")
  .get([authToken.verifyUser], deviceController.findOneDevice)
  .put([authToken.verifyAdmin], deviceController.updateDevice)
  .delete([authToken.verifyAdmin], deviceController.deleteDevice);

module.exports = router;